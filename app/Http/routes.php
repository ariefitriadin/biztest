<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'account'], function() {
    Route::any('{path?}', function () {
        return view("index");
    })->where("path", ".+");
});

Route::group(['prefix' => 'api'], function()
{
    Route::resource('authenticate/{page}', 'Auth\ApiAuthController', ['only' => ['index']]);
    Route::post('authenticate', 'Auth\ApiAuthController@authenticate');
    Route::get('authenticate/user/data', 'Auth\ApiAuthController@getAuthenticatedUser');
    Route::get('authenticate/user/first', 'Auth\ApiAuthController@firstUser');
});
