## biztest paginate

this code for test purpose


## Working Demo

This is the online working demo [working demo](http://biztest.onlinedemo.work/account).

## Installation Note

1. This Project using Laravel PHP Framework as Backend and Angular JS as Frontend

after clone the project :

2. using laravel Homestead [Laravel Homestead Doc](http://laravel.com/docs/5.1/homestead) : 
- create new folder somewhere in your drive

- edit /etc/hosts
   
  add 192.168.10.10 biztest.dev
  
- edit ~/.homestead/Homestead.yaml

  keep the rest default, except :  
  folders and site into something like this :
  
  folders : 
  
  - map /Users/username/biztest
  
  - to /home/vagrant/biztest
  
  
  sites :
   
  - map biztest.dev
  
  - to /home/vagrant/biztest/public
  
===
  
.env :

DB_HOST=localhost

DB_DATABASE=biztest

DB_USERNAME=homestead

DB_PASSWORD=secret
  
=== 
  
  
3. run vagrant up in new folder 
4. run vagrant ssh
5. in vagrant ssh : in folder /home/vagrant/biztest , run :

   - php artisan migrate
     
   - php artisan db:seed  (loadup Users table from randomuser.me)
   
   - serve biztest.dev /home/vagrant/biztest/public
   
   
6. in browser addressbar type : biztest.dev/account

   
DONE

Thanks
   
   
   
    
