(function() {

    'use strict';

    angular
        .module('bizApp')
        .controller('UserController',UserController);

    function UserController($http, $auth, $rootScope, $state,  uiGridConstants, $scope) {

        var vm = this;

        vm.users;
        vm.error;


        // We would normally put the logout method in the same
        // spot as the login method, ideally extracted out into
        // a service. For this simpler example we'll leave it here
        vm.logout = function() {

            $auth.logout().then(function() {

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');

                // Flip authenticated to false so that we no longer
                // show UI elements dependant on the user being logged in
                $rootScope.authenticated = false;

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;
                $state.go('auth');
            });
        }




        var paginationOptions = {
            pageNumber: 1,
            pageSize: 5,
            sort: null
        };

        $scope.gridOptions = {
            paginationPageSizes: [5, 10, 25],
            paginationPageSize: 5,
            useExternalPagination: true,
            useExternalSorting: true,
            columnDefs: [
                { field: 'pict', displayName:'',width:'25', cellTemplate:"<img width=\"25px\" ng-src=\"{{grid.getCellValue(row, col)}}\" lazy-src>"},
                { name: 'name' },
                { name: 'email', enableSorting: false },
                { name: 'phone', enableSorting: false }
            ],
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                    if (sortColumns.length == 0) {
                        paginationOptions.sort = null;
                    } else {
                        paginationOptions.sort = sortColumns[0].sort.direction;
                    }
                    getPage(paginationOptions.pageSize,paginationOptions.pageNumber);
                });
                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getPage(pageSize,newPage);
                });
            }
        };

        var getPage = function(psize,pagenum) {
            var url = 'api/authenticate/'+psize+'?page='+pagenum;

            $http.get(url)
                .success(function (data) {
                    $scope.gridOptions.totalItems = data.total;
                    $scope.gridOptions.data = data.data;
                    //var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
                    //$scope.gridOptions.data = data.data.slice(firstRow, firstRow + paginationOptions.pageSize);
                });
        };

        getPage(paginationOptions.pageSize,paginationOptions.pageNumber);


    }

})();